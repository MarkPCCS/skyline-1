# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.273');

# ---------------------------------------------------------------------- #
# Modify sp_part_stock_template                                          #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_template ADD COLUMN Sundry ENUM('Yes','No') NULL DEFAULT 'No' AFTER MakeUpTo, 
ADD COLUMN OEMPartNo VARCHAR(20) NULL DEFAULT NULL AFTER Sundry, 
ADD COLUMN OEMPartDesc VARCHAR(50) NULL DEFAULT NULL AFTER OEMPartNo, 
ADD COLUMN InitialQty INT(2) NULL DEFAULT '0' AFTER OEMPartDesc, 
ADD COLUMN ShelfLocation INT(10) NULL DEFAULT NULL AFTER InitialQty, 
ADD COLUMN BinLocation VARCHAR(10) NULL DEFAULT NULL AFTER ShelfLocation, 
ADD COLUMN PartUsageLimit INT(2) NULL DEFAULT NULL AFTER BinLocation, 
ADD COLUMN Accessory ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER PartUsageLimit, 
ADD COLUMN AccessoryCost DECIMAL(10,2) NOT NULL DEFAULT '0.0' AFTER Accessory, 
ADD COLUMN AccessoryWarrantyPeriod INT(2) NOT NULL DEFAULT '0' AFTER AccessoryCost, 
ADD COLUMN AccessorySerialNumber VARCHAR(50) NULL DEFAULT NULL AFTER AccessoryWarrantyPeriod, 
ADD COLUMN AllowDuplicatePartOnExchange ENUM('Yes','No') NULL DEFAULT 'No' AFTER AccessorySerialNumber, 
ADD COLUMN RetailItem ENUM('Yes','No') NULL DEFAULT 'No' AFTER AllowDuplicatePartOnExchange, 
ADD COLUMN SuspendPart ENUM('Yes','No') NULL DEFAULT 'No' AFTER RetailItem, 
ADD COLUMN RedundantPart ENUM('Yes','No') NULL DEFAULT 'No' AFTER SuspendPart, 
ADD COLUMN ReturnFaultySpare ENUM('Yes','No') NULL DEFAULT 'No' AFTER RedundantPart, 
ADD COLUMN ChargeablePartOnly ENUM('Yes','No') NULL DEFAULT 'No' AFTER ReturnFaultySpare, 
ADD COLUMN AttachBySolder ENUM('Yes','No') NULL DEFAULT 'No' AFTER ChargeablePartOnly, 
ADD COLUMN VATExempt ENUM('Yes','No') NULL DEFAULT 'No' AFTER AttachBySolder, 
ADD COLUMN SerialisedStock ENUM('Yes','No') NULL DEFAULT 'No' AFTER VATExempt, 
ADD COLUMN ShelfLifeExpiryDate DATE NULL DEFAULT NULL AFTER SerialisedStock, 
ADD COLUMN Weight DECIMAL(10.2) NULL DEFAULT '0.0' AFTER ShelfLifeExpiryDate, 
ADD COLUMN UseStockLevelTrigger ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER Weight, 
ADD COLUMN UseAutomaticReOrderPeriod ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER UseStockLevelTrigger, 
ADD COLUMN AutoReOrderPartEvery INT(3) NOT NULL DEFAULT '0' AFTER UseAutomaticReOrderPeriod, 
ADD COLUMN AutoReOrderQty INT(3) NOT NULL DEFAULT '0' AFTER AutoReOrderPartEvery, 
ADD COLUMN PercentageMarkupMainStore DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER AutoReOrderQty, 
ADD COLUMN UsePercentageMarkupForWarranty ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER PercentageMarkupMainStore, 
ADD COLUMN WarrantyPercentageMarkupValue DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER UsePercentageMarkupForWarranty, 
ADD COLUMN SalesPrice DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER WarrantyPercentageMarkupValue, 
ADD COLUMN UsePercentageMarkupForChargeable ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER SalesPrice, 
ADD COLUMN TradeCost DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER UsePercentageMarkupForChargeable, 
ADD COLUMN ChargeablePercentageMarkupValue DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER TradeCost, 
ADD COLUMN VATRateRepair INT(11) NULL DEFAULT NULL AFTER ChargeablePercentageMarkupValue, 
ADD COLUMN VATRateSale INT(11) NULL DEFAULT NULL AFTER VATRateRepair;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.274');
