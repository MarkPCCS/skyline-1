# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.208');

# ---------------------------------------------------------------------- #
# Modify Table service_provider_engineer	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider_engineer CHANGE COLUMN LunchPeriodFrom LunchPeriodFrom TIME NOT NULL DEFAULT '12:00:00' AFTER LunchBreakDuration, 
									  CHANGE COLUMN LunchPeriodTo LunchPeriodTo TIME NOT NULL DEFAULT '14:00:00' AFTER LunchPeriodFrom;
									  
									  
# ---------------------------------------------------------------------- #
# Modify Table non_skyline_job                                           #
# ---------------------------------------------------------------------- #
ALTER TABLE `non_skyline_job` ADD COLUMN `DateCreated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;


# ---------------------------------------------------------------------- #
# System Data	                                                         #
# ---------------------------------------------------------------------- #
INSERT INTO primary_fields (primaryFieldName) VALUES ('Model No');
				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.209');
