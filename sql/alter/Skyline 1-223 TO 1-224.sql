# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.223');


# ---------------------------------------------------------------------- #
# Add Table supplier                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE supplier ADD COLUMN BuildingNameNumber VARCHAR(50) NULL AFTER Status, 
					 ADD COLUMN Street VARCHAR(100) NULL AFTER BuildingNameNumber, 
					 ADD COLUMN LocalArea VARCHAR(100) NULL AFTER Street, 
					 ADD COLUMN TownCity VARCHAR(100) NULL AFTER LocalArea, 
					 ADD COLUMN CountryID INT(3) NULL AFTER TownCity;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.224');
