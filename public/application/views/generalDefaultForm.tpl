<script type="text/javascript">
    $("#BrandID").combobox();
    function sizeTextArea(field)
    {
        document.getElementById(field).style.height = document.getElementById(field).scrollHeight+"px";
    }
</script>    

{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <div id="generalDefaultFormPanel" class="SystemAdminFormPanel" >
    
        <form id="GDForm" name="GDForm" method="post"  action="#" class="inline">

        <fieldset>
            <legend title="" >{$form_legend|escape:'html'}</legend>

            <p><label id="suggestText" ></label></p>

            <p>
                <label ></label>
                <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
            </p>

            
           
            
            
            <p>
                <label class="fieldLabel" for="Category" >{$page['Labels']['category']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp; <input  type="text" class="text"  name="Category" value="{$datarow.Category|escape:'html'}" id="Category" >
            </p>
            <p>
                <label class="fieldLabel" for="DefaultName" >{$page['Labels']['default_name']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp; <input  type="text" class="text"  name="DefaultName" value="{$datarow.DefaultName|escape:'html'}" id="DefaultName" >
            </p>
            <p>
                <label class="fieldLabel" for="Default" >{$page['Labels']['default']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp; <input  type="text" class="text"  name="Default" value="{$datarow.Default|escape:'html'}" id="Default" >
            </p>
            <p>
                <label class="fieldLabel" for="Descrpition" >{$page['Labels']['description']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp; <textarea name="Description" id="Description" class="text text_area">{$datarow.Description|escape:'html'}</textarea>

            </p>
            <p>
                <label class="fieldLabel" for="BrandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp;
                <select name="BrandID" id="BrandID" >
                    <option value="" {if $datarow.BrandID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                    {foreach $brands as $brand}
                        <option value="{$brand.BrandID|escape:'html'}" {if $datarow.BrandID eq $brand.BrandID} selected="selected" {/if}>{$brand.Name|escape:'html'} ({$brand.BrandID|escape:'html'}) </option>
                    {/foreach}
                </select>
            </p>
            <p>
                <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                &nbsp;&nbsp;
                {foreach $statuses as $status}
                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 
                {/foreach}
            </p>

            <p>

                    <span class= "bottomButtons" >

                        <input type="hidden" name="GeneralDefaultID" id="GeneralDefaultID" value="{$datarow.GeneralDefaultID|escape:'html'}" >
                        <input type="hidden" name="CreatedDate" id="CreatedDate" value="{$datarow.CreatedDate|escape:'html'}" >
                        <input type="hidden" name="ModifiedUserID" id="ModifiedUserID" value="{$datarow.ModifiedUserID|escape:'html'}" >
                        <input type="hidden" name="ModifiedDate" id="ModifiedUserDate" value="{$datarow.ModifiedDate|escape:'html'}" >
                        <input type="hidden" name="OldStatus" id="OldStatus" value="{$datarow.Status|escape:'html'}" >
                        
                        <input  type="hidden"  name="IDNo" value="{$datarow.IDNo|escape:'html'}" id="IDNo" >
                        
                         
                        
                        
                        {if $datarow.GeneralDefaultID neq '' && $datarow.GeneralDefaultID neq '0'}
                            <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                        {else}
                           <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                        {/if}

                        <br>
                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                    </span>

                </p> 

        </fieldset>    

    </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
