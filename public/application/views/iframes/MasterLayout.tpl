<!DOCTYPE html>

    {*********************************************************************
     *
     * Define Smarty Global Varables
     *
     *********************************************************************}
     
    {$Title = 'Skyline Job Booking'}
        
    {block name=config}
    {/block}   
   
    {***********************************************************************}
    
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
    <head>
        <meta charset="utf-8">
        
        {* Always force latest IE rendering engine (even in intranet) & Chrome Frame  *}
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">   
        
        <meta name="title" content="">
        <meta name="description" content="">
        {* Google will often use this as its description of your page/site. Make it good. *}
	
        <meta name="google-site-verification" content="">
       {* Speaking of Google, don't forget to set your site up: http://google.com/webmasters *}
	
        <meta name="author" content="Your Name Here">
        <meta name="Copyright" content="Copyright Your Name Here 2011. All Rights Reserved.">

        {* Dublin Core Metadata : http://dublincore.org/ *}
        <meta name="DC.title" content="Project Name">
        <meta name="DC.subject" content="What you're about.">
        <meta name="DC.creator" content="Who made this site.">
        
        <title>{$Title|escape:"html"}</title>

        {* Place favicon.ico and apple-touch-icon.png in the root directory  *}
        <link rel="shortcut icon" href="{$_subdomain}/favicon.ico" type="image/x-icon" />
             
        {block name=styles}
        {/block}
        
        {* Site Stylesheet  *}
        <link rel="stylesheet" href="{$_subdomain}/iframe/css/style.css" type="text/css"  />
        
        {* JQuery UI Stylesheet  *}
        <link rel="stylesheet" href="{$_subdomain}/iframe/css/jquery-ui-1.10.3.custom.min.css" type="text/css"  />
        
        {* Skin Stylesheet  *}
        {if $_skin ne ''}
        <link rel="stylesheet" href="{$_subdomain}/iframe{$_skin}/css/style.css" type="text/css"  />
        {/if}
        
        {* Load Modernizer script...  *}
        <script src="{$_subdomain}/iframe/js/modernizr-2.6.2.min.js"></script>
        
        {* Load JQuery Tools script...  *}
        <script src="{$_subdomain}/iframe/js/jquery.tools.min.js"></script>
        <script src="{$_subdomain}/iframe/js/plugins.js"></script>  
        
        {* Load JQuery UI script...  *}
        <script src="{$_subdomain}/iframe/js/jquery-ui-1.10.3.custom.min.js"></script> 
        
        {block name=scripts}
        {/block}
        
    </head>
    <body> 
        {* NOTE: use $_subdomain in root url references to allow for website root in a sub-folder of apache root *}   
        {* NOTE: use $_skin in image url references to allow for customised skins 
                  e.g. <img src="{$_subdomain}/iframe{$_skin}/images/prev.png" id="prev"><
                 images will load from /iframe/images if not present in skins image folder - see .htaccess file for details *}
                 
        {block name=body}
        {/block}    
{* ***********************************************************************
    
    IMPORTANT NOTE: The closing body and html tags are now appended to the output stream
                    in index.php.
         
    </body>
                
</html>

**************************************************************************** *}