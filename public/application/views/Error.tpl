{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "$Title - Error"}
{$PageId = $ErrorPage}
{/block}

{block name=scripts}
{/block}

{block name=body}
<div class="main" id="error">
    
<h2>
    The page you were looking for appears to have been moved,<br /> 
    deleted or does not exist.
</h2>
<p>
    <a href="{$_subdomain}/index/index">Please try returning to the Home Page 
    to continue.</a>
</p>
<h3>Sorry for your inconvenience.</h3>
 
</div>
{/block}