<?php

/**
 * TableSQL Class
 *
 * Description.....
 *
 * @author      Brian Etherington <brian@smokingun.co.uk>
 * @copyright   2011 Smokingun Graphics
 * @link        http://www.smokingun.co.uk
 * @version     1.2
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version   
 * 28/08/2012  1.1     Brian Etherington     Added getters
 * 24/04/2013  1.2     Brian Etherington     Add FindCommand
 ******************************************************************************/

class TableSQL {
    
    /**
     * Contains the Table name
     *
     * @var string
     * @access private
     */
    private $table;
    
    /**
     * Contains the Primary Key name
     *
     * @var string
     * @access private
     */
    private $primaryKey;

    /**
     * is the primary key Auto Increment
     *
     * @var bool
     * @access private
     */
    private $isAutoinc;
    
    /**
     * Constructor for initializing the class with default values.
     * 
     * @param  string  $table       The table name
     * @param  string  $primaryKey  A comma separated list of Primary Key Column Names
     * @param  boolean $isAutoinc   true if Primary Key is AutoIncrement
     * @return void  
     */
    public function __construct( $table, $primaryKey, $isAutoinc = true) {
                
        $this->table      = $table; 
        $this->primaryKey = explode(',',$primaryKey);
        $this->isAutoinc  = $isAutoinc;
        
    }
    
    public function getPrimaryKey() {
        return $this->primaryKey;
    }

    public function getIsAutoinc() {
        return $this->isAutoinc;
    }
    
    /**
     * Generate Find Command SQL
     * 
     * Description..... 
     * 
     * Example use:
     * 
     * <code>

     * </code>
     *
     * @param  array   $params         Column names as associative array names=>values
     * @return string  $findCommand    The generated SQL string.
     */    
    public function findCommand( $params ) {
        
        $where = '';
        $and = '';
        
        foreach($params as $key => $v) {
                $where .= "$and `$key`= :$key";
                $and = ' AND';
        }
        
        if ($where == '')
            $sql = "SELECT * from `$this->table`";
        else
            $sql = "SELECT * FROM `$this->table` WHERE $where";
        
        return $sql;
        
    }

    /**
     * Generate Insert Command SQL
     * 
     * Description..... 
     * 
     * Example use:
     * 
     * <code>

     * </code>
     *
     * @param  array   $params         Column names as associative array names=>values
     * @return string  $insertCommand  The generated SQL string.
     */    
    public function insertCommand( $params ) {
        
        $cols = '';
        $values = '';
        $sep = '';
        
        foreach($params as $key => $v) {
            if (!($this->isAutoinc && in_array($key, $this->primaryKey))) {
                $cols .= "$sep`$key`";
                $values .= "$sep:$key";
                $sep = ',';
            }
        }
        
        return "INSERT INTO $this->table ($cols) VALUES($values);";
        
    }
    
    /**
     * Generate Update Command SQL
     * 
     * Description..... 
     * 
     * Example use:
     * 
     * <code>
//        $sql = TableFactory::Job()->updateCommand(array('ItemLocation'=>'ItemLocation'));
//        
//        $update = $this->conn->prepare( $sql );
//        
//        $update->execute( array(
//            ':JobID' => '34589120',
//            ':ItemLocation' => 'Test'
//        ) );
     * </code>
     *
     * @param  array   $params         Column names as associative array names=>values
     * @param  string  $where          SQl where condition
     * @return string  $updateCommand  The generated SQL string.
     */    
    public function updateCommand( $params, $where=null ) {
        
        $sql = "UPDATE $this->table SET ";
        
        $sep = '';
        foreach ($params as $key => $v) { 
            if (!in_array($key, $this->primaryKey)) {
                $sql .= "$sep`$key`= :$key";
                $sep = ',';
            }
        } 
        
        $sql .= $this->whereClause( $where );
        
        return $sql;

    }
    
    /**
     * Generate Delete Command SQL
     * 
     * Description..... 
     * 
     * Example use:
     * 
     * <code>

     * </code>
     *
     * @param  string $where          SQl where condition
     * @return string $deleteCommand  The generated SQL string.
     */    
    public function deleteCommand( $where=null ) {
        
        return "DELETE FROM $this->table" . $this->whereClause( $where ); 

    }
    
    /**
     * Generate WHERE clause.
     * 
     * Description..... 
     *
     * @access private
     * @param  string $where  SQl where condition
     * @return string $sql    The generated SQL where clause.
     */
    private function whereClause ( $where ) {
                
        if (is_null($where) || $where == '') {
            $sql = ' WHERE '; 
            $sep = '';
            foreach($this->primaryKey as $key) {
                $sql .= "$sep`$key`=:$key";
                $sep = ' AND ';  
            }
            $sql .= ';';
        } else if (is_string($where)) {
            $sql = " WHERE $where;";
        } else {
            throw new Exception("Table $this->table Invalid WHERE data type.");
        }
        
        return $sql;
        
    }
    
}

?>